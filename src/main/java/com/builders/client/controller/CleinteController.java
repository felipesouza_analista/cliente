package com.builders.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.builders.client.domain.model.Cliente;
import com.builders.client.service.ClienteService;
import com.github.fge.jsonpatch.JsonPatch;

@RestController
@RequestMapping("/clientes")
public class CleinteController {

	@Autowired
	private ClienteService service;
	
	@GetMapping
	public ResponseEntity<Object> clienteFindAll() {
		return service.findAll();
	}
	
	@GetMapping("/busca")
	public ResponseEntity<Object> clienteFindAllByNameOrCpf(@RequestParam(required = false) String nome, @RequestParam(required = false) String cpf) {
		return service.findAllSorted(nome, cpf);
	}
	
	@PostMapping
	public ResponseEntity<Object> clienteSave(@RequestBody Cliente cliente) {
		return service.save(cliente);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Object> clienteDelete(@PathVariable Long id) {
		return service.delete(id);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Object> clienteUpdate(@PathVariable Long id, @RequestBody Cliente cliente) {
		return service.update(id, cliente);
	}
	
	@PatchMapping(path = "/{id}", consumes = "application/json-patch+json")
	public ResponseEntity<Object> clienteUpdateParts(@PathVariable Long id, @RequestBody JsonPatch patch) {
		return service.updateParts(id, patch);
	}

}
