package com.builders.client.domain.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.builders.client.domain.model.Cliente;

@Repository
public interface ClienteRepository extends PagingAndSortingRepository<Cliente, Long> {
	
	Page<Cliente> findAllByNomeIgnoreCaseContainingOrCpfIgnoreCaseContaining(String nome, String cpf, Pageable page);
}
