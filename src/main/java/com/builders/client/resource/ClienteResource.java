package com.builders.client.resource;

import java.time.LocalDate;
import java.time.Period;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;

import com.builders.client.domain.model.Cliente;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Component
@NoArgsConstructor
public class ClienteResource {

	private Long id;
	private String nome;
	private String cpf;
	private LocalDate dataNascimento;
	private Integer idade;
	
	public ClienteResource(Cliente cliente) {
		super();
		this.id = cliente.getId();
		this.nome = cliente.getNome();
		this.cpf = cliente.getCpf();
		this.dataNascimento = cliente.getDataNascimento();
		if(cliente.getDataNascimento() != null) {
			this.idade = Period.between(cliente.getDataNascimento(), LocalDate.now()).getYears();
		} else {
			this.idade = 0;
		}
	}
	
	public List<ClienteResource> toList(List<Cliente> list) {
		
		List<ClienteResource> resource = list.stream().map(cliente -> new ClienteResource(cliente)).collect(Collectors.toList());
		return resource;
	}
	
	
}
