package com.builders.client.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.builders.client.domain.model.Cliente;
import com.builders.client.domain.repository.ClienteRepository;
import com.builders.client.resource.ClienteResource;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.fge.jsonpatch.JsonPatch;
import com.github.fge.jsonpatch.JsonPatchException;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository repository;
	
	@Autowired
	private ClienteResource resource;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	public ResponseEntity<Object> findAll() {
		
		List<ClienteResource> response = resource.toList(repository.findAll(PageRequest.of(0, 10)).getContent());
		
		Page<ClienteResource> page = new PageImpl<ClienteResource>(response, PageRequest.of(0, 10), response.size());
		
		return ResponseEntity.ok().body(page);
	}
	
	public ResponseEntity<Object> findAllSorted(String nome, String cpf) {
		
		List<ClienteResource> response = resource.toList(repository.findAllByNomeIgnoreCaseContainingOrCpfIgnoreCaseContaining(nome, cpf, PageRequest.of(0, 10)).getContent());
		
		Page<ClienteResource> page = new PageImpl<ClienteResource>(response, PageRequest.of(0, 10), response.size());
		
		return ResponseEntity.ok().body(page);
	}
	
	public ResponseEntity<Object> save(Cliente cliente) {
		try {
			return ResponseEntity.created(null).body(repository.save(cliente));
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Cliente com informações incorretas");
		}
	}
	
	public ResponseEntity<Object> delete(Long id) {
		try {
			repository.deleteById(id);
			return ResponseEntity.noContent().build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Cliente com id = "+id+" não encontrado.");
		}
	}
	
	public ResponseEntity<Object> update(Long id, Cliente cliente) {
		Optional<Cliente> optional = repository.findById(id);
		if(optional.isPresent()) {
			cliente.setId(id);
			return ResponseEntity.ok().body(repository.save(cliente));
		} else {
			return ResponseEntity.badRequest().body("Cliente com id = "+id+" não encontrado.");
		}
	}
	
	public ResponseEntity<Object> updateParts(Long id, JsonPatch patch) {
		try {
			Cliente cliente = repository.findById(id).orElseThrow(Exception::new);
			Cliente clientePatch = applyPatchToCustomer(patch, cliente);
			return ResponseEntity.ok().body(repository.save(clientePatch));
		} catch (JsonProcessingException | JsonPatchException e) {
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
		} catch (Exception e) {
			return ResponseEntity.badRequest().body("Cliente com id = "+id+" não encontrado.");
		}
	}
	
	private Cliente applyPatchToCustomer(JsonPatch patch, Cliente cliente) throws JsonPatchException, JsonProcessingException {
		
        JsonNode patched = patch.apply(objectMapper.convertValue(cliente, JsonNode.class));
        
        return objectMapper.treeToValue(patched, Cliente.class);
    }

}